﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShapeAnimator.Model.Shapes;
using ShapeAnimator.Model.Utility;

namespace ShapeAnimatorTest
{
    /// <summary>
    ///     Tests for when calculating the area of shapes.
    /// </summary>
    [TestClass]
    public class WhenCalculatingArea
    {
        #region Constants

        private const double Delta = .01;
        private const int NumberOfAsserts = 100000;

        #endregion

        #region Test Methods

        /// <summary>
        ///     A square the width of 30 and height of 55 should have a 1650 area.
        /// </summary>
        [TestMethod]
        public void SquareWidth30Width55HeightShouldHave1650Area()
        {
            const int expectedArea = 1650;

            const int width = 30;
            const int height = 55;

            const int actualArea = width*height;

            Assert.AreEqual(expectedArea, actualArea);
        }

        /// <summary>
        ///     A circle the width 25 width and 75 height should have about a 1473 area.
        /// </summary>
        [TestMethod]
        public void CircleWidth25Width75HeightShouldHaveAbout1473Area()
        {
            const double expectedArea = 1472.62;

            const int width = 25;
            const int height = 75;

            const double semiMajorAxis = (double) width/2;
            const double semiMinorAxis = (double) height/2;

            const double actualArea = Math.PI*semiMajorAxis*semiMinorAxis;

            Assert.AreEqual(expectedArea, actualArea, Delta);
        }

        /// <summary>
        ///     Areas the of square should between 400 inclusive and 10000 inclusive.
        /// </summary>
        [TestMethod]
        public void AreaOfSquareShouldBetween400InclusiveAnd10000Inclusive()
        {
            const int minimumArea = 400;
            const int maximumArea = 10000;

            for (int i = 0; i < NumberOfAsserts; i++)
            {
                var testSquare = new Square();

                double area = ShapeSizeCalculator.GetAreaOfShape(testSquare);

                Assert.IsTrue(area >= minimumArea && area <= maximumArea);
            }
        }

        /// <summary>
        ///     Areas the of circle should between about 314 inclusive and aobut 7854 inclusive.
        /// </summary>
        [TestMethod]
        public void AreaOfCircleShouldBetweenAbout314InclusiveAndAbout7854Inclusive()
        {
            const int minimumArea = 314;
            const int maximumArea = 7854;

            for (int i = 0; i < NumberOfAsserts; i++)
            {
                var testCircle = new Circle();

                double area = ShapeSizeCalculator.GetAreaOfShape(testCircle);

                Assert.IsTrue(area >= minimumArea && area <= maximumArea);
            }
        }

        /// <summary>
        ///     Areas the of spotted circle should between about 314 inclusive and about 7854 inclusive.
        /// </summary>
        [TestMethod]
        public void AreaOfSpottedCircleShouldBetweenAbout314InclusiveAndAbout7854Inclusive()
        {
            const int minimumArea = 314;
            const int maximumArea = 7854;

            for (int i = 0; i < NumberOfAsserts; i++)
            {
                var testSpottedCircle = new SpottedCircle();

                double area = ShapeSizeCalculator.GetAreaOfShape(testSpottedCircle);

                Assert.IsTrue(area >= minimumArea && area <= maximumArea);
            }
        }

        #endregion
    }
}