﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShapeAnimator.Model.Shapes;
using ShapeAnimator.Model.Utility;

namespace ShapeAnimatorTest
{
    /// <summary>
    ///     Test for when calculating perimeter of shapes.
    /// </summary>
    [TestClass]
    public class WhenCalculatingPerimeter
    {
        #region Constants

        private const double Delta = .01;

        private const double NumberOfAsserts = 100000;

        #endregion

        #region Test Methods

        /// <summary>
        ///     The perimeter of a square with a 20 width and 45 height should be 130.
        /// </summary>
        [TestMethod]
        public void PerimeterOfASquareWith20Width45HeightShouldBe130()
        {
            const int expectedPerimeter = 130;

            const int width = 20;
            const int height = 45;

            const int actualPerimeter = 2*(height + width);

            Assert.AreEqual(expectedPerimeter, actualPerimeter);
        }

        /// <summary>
        ///     The perimeter of a circle with a 33 width and 66 height should be about 160.
        /// </summary>
        [TestMethod]
        public void PerimeterOfACircleWith33Width66HeightShouldBeAbout160()
        {
            const double expectedPerimeter = 159.86;

            const int width = 33;
            const int height = 66;

            const double semiMajorAxis = (double) width/2;
            const double semiMinorAxis = (double) height/2;

            double actualPerimeter = Math.PI*
                                     (3*(semiMajorAxis + semiMinorAxis) -
                                      Math.Sqrt((3*semiMajorAxis + semiMinorAxis)*(semiMajorAxis + 3*semiMinorAxis)));

            Assert.AreEqual(expectedPerimeter, actualPerimeter, Delta);
        }

        /// <summary>
        ///     The perimeters of a square should be between80 inclusive and 400 inclusive.
        /// </summary>
        [TestMethod]
        public void PerimeterOfASquareShouldBeBetween80InclusiveAnd400Inclusive()
        {
            const int minimumPerimeter = 80;
            const int maximumPerimeter = 400;

            for (int i = 0; i < NumberOfAsserts; i++)
            {
                var testSquare = new Square();

                double perimeter = ShapeSizeCalculator.GetPerimeterOfShape(testSquare);

                Assert.IsTrue(perimeter >= minimumPerimeter && perimeter <= maximumPerimeter);
            }
        }

        /// <summary>
        ///     The perimeters of a square should be between80 inclusive and 400 inclusive.
        /// </summary>
        [TestMethod]
        public void PerimeterOfACircleShouldBeBetweenAbout62InclusiveAndAbout314Inclusive()
        {
            const double minimumPerimeter = 62.8;
            const double maximumPerimeter = 314.16;

            for (int i = 0; i < NumberOfAsserts; i++)
            {
                var testCircle = new Circle();

                double perimeter = ShapeSizeCalculator.GetPerimeterOfShape(testCircle);

                Assert.IsTrue(perimeter >= minimumPerimeter && perimeter <= maximumPerimeter);
            }
        }

        /// <summary>
        ///     The perimeters of a square should be between80 inclusive and 400 inclusive.
        /// </summary>
        [TestMethod]
        public void PerimeterOfASpottedCircleShouldBeBetweenAbout62InclusiveAndAbout314Inclusive()
        {
            const double minimumPerimeter = 62.8;
            const double maximumPerimeter = 314.16;

            for (int i = 0; i < NumberOfAsserts; i++)
            {
                var testSpottedCircle = new SpottedCircle();

                double perimeter = ShapeSizeCalculator.GetPerimeterOfShape(testSpottedCircle);

                Assert.IsTrue(perimeter >= minimumPerimeter && perimeter <= maximumPerimeter);
            }
        }

        #endregion
    }
}