﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using ShapeAnimator.Model.Shapes;

namespace ShapeAnimator.Model.Utility
{
    /// <summary>
    ///     Creates random objects and values.
    /// </summary>
    public static class Randomizer
    {
        #region Instance variables

        private static readonly Random RandomNumberGenerator = new Random();

        #endregion

        #region Constants

        private const int IndexOfFirstElement = 0;

        private const int MininumColorValue = 0;
        private const int MaximumColorValue = 256;

        #endregion

        /// <summary>
        ///     Gets a nonnegative random number less than the maximum value.
        ///     Precondition: None
        /// </summary>
        /// <param name="maxValue">The maximum value.</param>
        /// <returns></returns>
        public static int GetRandomNumber(int maxValue)
        {
            return RandomNumberGenerator.Next(maxValue);
        }

        /// <summary>
        ///     Gets a random number within a certain range.
        ///     Precondition: None
        /// </summary>
        /// <param name="minValue">The minimum value.</param>
        /// <param name="maxValue">The maximum value.</param>
        /// <returns></returns>
        public static int GetRandomNumber(int minValue, int maxValue)
        {
            return RandomNumberGenerator.Next(minValue, maxValue);
        }

        /// <summary>
        ///     Gets a random shape.
        /// </summary>
        /// <returns>A shape</returns>
        public static Shape GetRandomShape()
        {
            var shapes = new List<Shape> {new Circle(), new SpottedCircle(), new Square()};

            List<Shape> shuffledShapes = shapes.OrderBy(shape => RandomNumberGenerator.Next()).ToList();

            Shape aShape = shuffledShapes[IndexOfFirstElement];

            return aShape;
        }

        /// <summary>
        ///     Gets the random color.
        /// </summary>
        /// <returns>A random color.</returns>
        public static Color GetRandomColor()
        {
            Color aColor = Color.FromArgb(RandomNumberGenerator.Next(MininumColorValue, MaximumColorValue),
                RandomNumberGenerator.Next(MininumColorValue, MaximumColorValue),
                RandomNumberGenerator.Next(MininumColorValue, MaximumColorValue));

            return aColor;
        }
    }
}