﻿using System;
using ShapeAnimator.Model.Shapes;

namespace ShapeAnimator.Model.Utility
{
    /// <summary>
    ///     Calculates the area and perimeter of a shape.
    /// </summary>
    public static class ShapeSizeCalculator
    {
        #region Constants

        private const int Digits = 3;

        #endregion

        #region Methods

        /// <summary>
        ///     Gets the area of shape.
        /// </summary>
        /// <param name="theShape">The shape.</param>
        /// <returns>The area of the shape.</returns>
        /// <exception cref="System.ArgumentNullException">theShape</exception>
        public static double GetAreaOfShape(Shape theShape)
        {
            if (theShape == null)
            {
                throw new ArgumentNullException("theShape");
            }

            if (theShape is Square)
            {
                return theShape.Width*theShape.Height;
            }

            double semiMajorAxis = (double) theShape.Width/2;
            double semiMinorAxis = (double) theShape.Height/2;

            return Math.Round(Math.PI*semiMajorAxis*semiMinorAxis, 3);
        }

        /// <summary>
        ///     Gets the perimeter of shape.
        /// </summary>
        /// <param name="theShape">The shape.</param>
        /// <returns>The perimeter of the shape.</returns>
        /// <exception cref="System.ArgumentNullException">theShape</exception>
        public static double GetPerimeterOfShape(Shape theShape)
        {
            if (theShape == null)
            {
                throw new ArgumentNullException("theShape");
            }

            if (theShape is Square)
            {
                return 2*(theShape.Height + theShape.Width);
            }

            double semiMajorAxis = (double) theShape.Width/2;
            double semiMinorAxis = (double) theShape.Height/2;

            return Math.Round(Math.PI*(3*(semiMajorAxis + semiMinorAxis) -
                                       Math.Sqrt((3*semiMajorAxis + semiMinorAxis)*(semiMajorAxis + 3*semiMinorAxis))),
                Digits);
        }

        #endregion
    }
}