﻿using System.Collections.Generic;
using System.Linq;
using ShapeAnimator.Model.Shapes;

namespace ShapeAnimator.Model.Utility
{
    /// <summary>
    ///     Sorts a collection of shapes.
    /// </summary>
    public static class ShapeSorter
    {
        /// <summary>
        ///     Sorts and returns the collection of shapes.
        /// </summary>
        /// <param name="shapes">The shapes.</param>
        /// <returns>The sorted shapes. </returns>
        public static IEnumerable<Shape> SortShapes(List<Shape> shapes)
        {
            return from shape in shapes
                   orderby shape.Perimeter, shape.Area, shape.GetType().Name, shape.TheColor.ToArgb(), shape.CollisionCount
                   select shape;
        }
    }
}
