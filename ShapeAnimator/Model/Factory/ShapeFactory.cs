﻿using ShapeAnimator.Model.Shapes;
using ShapeAnimator.Model.Utility;

namespace ShapeAnimator.Model.Factory
{
    /// <summary>
    ///     Creates different kinds of shapes.
    /// </summary>
    public static class ShapeFactory
    {
        #region Methods

        /// <summary>
        ///     Creates a random shape.
        /// </summary>
        /// <returns>A shape</returns>
        public static Shape CreateRandomShape()
        {
            return Randomizer.GetRandomShape();
        }

        /// <summary>
        ///     Creates a circle.
        /// </summary>
        /// <returns>A circle.</returns>
        public static Shape CreateACircle()
        {
            return new Circle();
        }

        /// <summary>
        ///     Creates a spotted circle.
        /// </summary>
        /// <returns>A spotted circle.</returns>
        public static Shape CreateASpottedCircle()
        {
            return new SpottedCircle();
        }

        /// <summary>
        ///     Creates a square.
        /// </summary>
        /// <returns>A square.</returns>
        public static Shape CreateASquare()
        {
            return new Square();
        }

        #endregion
    }
}