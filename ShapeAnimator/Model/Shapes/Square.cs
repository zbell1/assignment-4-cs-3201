using System;
using System.Drawing;
using ShapeAnimator.View.Shapes;

namespace ShapeAnimator.Model.Shapes
{
    /// <summary>
    ///     Holds information about a square.
    /// </summary>
    public class Square : Shape
    {
        #region Constructors

        /// <summary>
        ///     Default constructor for a square.
        ///     Postcondition: TheSprite == new SquareSprite
        /// </summary>
        public Square()
        {
            this.TheSprite = new SquareSprite(this);
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Draws a square.
        ///     Precondition: g != null
        /// </summary>
        /// <param name="g">The graphics object used to draw the shape.</param>
        public override void Paint(Graphics g)
        {
            if (g == null)
            {
                throw new ArgumentNullException("g");
            }

            this.TheSprite.Paint(g);
        }

        #endregion
    }
}