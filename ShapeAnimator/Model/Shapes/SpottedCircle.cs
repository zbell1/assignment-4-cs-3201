using System;
using System.Drawing;
using ShapeAnimator.View.Shapes;

namespace ShapeAnimator.Model.Shapes
{
    /// <summary>
    ///     Holds information about a spotted circle.
    /// </summary>
    public class SpottedCircle : Shape
    {
        #region Constructors

        /// <summary>
        ///     Default constructor for a spotted circle.
        ///     Postcondition: TheSprite == new SpottedCircleSprite
        /// </summary>
        public SpottedCircle()
        {
            this.TheSprite = new SpottedCircleSprite(this);
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Draws a spotted circle.
        ///     Precondition: g != NULL
        /// </summary>
        /// <param name="g">The graphics object used to draw the shape.</param>
        public override void Paint(Graphics g)
        {
            if (g == null)
            {
                throw new ArgumentNullException("g");
            }

            this.TheSprite.Paint(g);
        }

        #endregion
    }
}