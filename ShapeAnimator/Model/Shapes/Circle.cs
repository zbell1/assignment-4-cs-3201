﻿using System;
using System.Drawing;
using ShapeAnimator.View.Shapes;

namespace ShapeAnimator.Model.Shapes
{
    /// <summary>
    ///     Holds information about a circle.
    /// </summary>
    public class Circle : Shape
    {
        #region Constructors

        /// <summary>
        ///     Default constructor for a circle.
        ///     Postcondition: TheSprite == new CircleSprite
        /// </summary>
        public Circle()
        {
            this.TheSprite = new CircleSprite(this);
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Draws a circle
        ///     Precondition: g != NULL
        /// </summary>
        /// <param name="g">The graphics object used to draw the shape.</param>
        public override void Paint(Graphics g)
        {
            if (g == null)
            {
                throw new ArgumentNullException("g");
            }
            this.TheSprite.Paint(g);
        }

        #endregion
    }
}