﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using ShapeAnimator.Model.Utility;
using ShapeAnimator.View.Shapes;

namespace ShapeAnimator.Model.Shapes
{
    /// <summary>
    ///     Holds information about a shape
    /// </summary>
    public abstract class Shape
    {
        #region Instance variables

        private readonly Color theColor;

        private Rectangle boundingBox;

        #endregion

        #region Constants

        private const int MinimumLength = 20;
        private const int MaximumLength = 101;
        private const int MinimumSpeed = 1;
        private const int MaximumSpeed = 6;
        private const int NumberOfDirectionTypes = 2;
        private const int NegativeDirection = -1;
        private const int PositiveDirection = 1;
        private const int CollisionCounter = 1;

        /// <summary>
        ///     Used to make the shape travel in the opposite direction.
        /// </summary>
        public readonly int OppositeDirection = -1;

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the sprite.
        /// </summary>
        /// <value>
        ///     The sprite.
        /// </value>
        protected ShapeSprite TheSprite { get; set; }

        /// <summary>
        ///     Gets or sets the x location of the shape.
        /// </summary>
        /// <value>
        ///     The x-coordinate.
        /// </value>
        public int X
        {
            get { return this.boundingBox.X; }
            set { this.boundingBox.X = value; }
        }

        /// <summary>
        ///     Gets or sets the y location of the shape.
        /// </summary>
        /// <value>
        ///     The y-coordinate.
        /// </value>
        public int Y
        {
            get { return this.boundingBox.Y; }
            set { this.boundingBox.Y = value; }
        }

        /// <summary>
        ///     Gets the width of the shape.
        /// </summary>
        public int Width
        {
            private set { this.boundingBox.Width = value; }
            get { return this.boundingBox.Width; }
        }

        /// <summary>
        ///     Gets the height of the shape.
        /// </summary>
        public int Height
        {
            private set { this.boundingBox.Height = value; }
            get { return this.boundingBox.Height; }
        }

        /// <summary>
        ///     Gets or sets the perimeter of the shape.
        /// </summary>
        /// <value>
        ///     The perimeter.
        /// </value>
        public double Perimeter { private set; get; }

        /// <summary>
        ///     Gets or sets the area of the shape..
        /// </summary>
        /// <value>
        ///     The area.
        /// </value>
        public double Area { private set; get; }

        /// <summary>
        ///     Gets or sets the speed at which
        ///     the shape travels in the X direction.
        /// </summary>
        /// <value>
        ///     The speed in the X direction.
        /// </value>
        private int XSpeed { get; set; }

        /// <summary>
        ///     Gets or sets the speed at which
        ///     the shape travels in the Y direction.
        /// </summary>
        /// <value>
        ///     The speed in the Y direction.
        /// </value>
        private int YSpeed { get; set; }

        /// <summary>
        ///     Gets the color.
        /// </summary>
        /// <value>
        ///     The color.
        /// </value>
        public Color TheColor
        {
            get { return this.theColor; }
        }

        /// <summary>
        ///     Gets the number of times the shape has collided with the canvas.
        /// </summary>
        public int CollisionCount { private set; get; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Default constructor for a shape.
        ///     Postcondition: theColor == a random color.
        ///     Postcondition: Width == A random width between the minimum and maximum length.
        ///     Postcondition: Height == A random height between the mininmum and maximum length.
        ///     Postcondition: XSpeed == A random speed with a random direction.
        ///     Postcondition: YSpeed == A random speed with a random direction.
        /// </summary>
        protected Shape()
        {
            this.theColor = Randomizer.GetRandomColor();

            this.setSize();

            this.setSpeedsAndDirection();
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Draws a shape
        ///     Precondition: g != NULL
        /// </summary>
        /// <param name="g">The graphics object used to draw the shape.</param>
        public abstract void Paint(Graphics g);

        /// <summary>
        ///     Moves the shape.
        ///     Postcondition: X == X@prev + XSpeed, Y == Y@prev + YSpeed
        ///     Postcondition: boundingBox.X == X, boundingBox.Y == Y
        /// </summary>
        public void Move()
        {
            this.X += this.XSpeed;
            this.Y += this.YSpeed;

            this.boundingBox.X = this.X;
            this.boundingBox.Y = this.Y;
        }

        /// <summary>
        ///     Bounces the shape in the opposite direction at the canvas boundaries.
        ///     Precondition: canvas!= null;
        ///     Postcondition: XSpeed == XSpeed@prev * OppositeDirection,  YSpeed == YSpeed@prev * OppositeDirection
        ///     Postcondition: CollisionCount == CollisonCount@prev + CollisonCounter;
        /// </summary>
        /// <param name="canvas">the canvas</param>
        public void BounceInOppositeDirectionAtCanvasBounds(PictureBox canvas)
        {
            if (canvas == null)
            {
                throw new ArgumentNullException("canvas");
            }

            if (this.boundingBox.Left <= canvas.DisplayRectangle.Left ||
                this.boundingBox.Right >= canvas.DisplayRectangle.Right)
            {
                this.XSpeed = this.XSpeed*this.OppositeDirection;

                this.CollisionCount += CollisionCounter;
            }

            if (this.boundingBox.Bottom >= canvas.DisplayRectangle.Bottom ||
                this.boundingBox.Top <= canvas.DisplayRectangle.Top)
            {
                this.YSpeed = this.YSpeed*this.OppositeDirection;

                this.CollisionCount += CollisionCounter;
            }
        }

        private void setSize()
        {
            this.Width = Randomizer.GetRandomNumber(MinimumLength, MaximumLength);
            this.Height = Randomizer.GetRandomNumber(MinimumLength, MaximumLength);

            this.Perimeter = ShapeSizeCalculator.GetPerimeterOfShape(this);
            this.Area = ShapeSizeCalculator.GetAreaOfShape(this);
        }

        private void setSpeedsAndDirection()
        {
            var directions = new List<int> {NegativeDirection, PositiveDirection};

            this.XSpeed = Randomizer.GetRandomNumber(MinimumSpeed, MaximumSpeed);
            this.XSpeed = this.XSpeed*directions[Randomizer.GetRandomNumber(NumberOfDirectionTypes)];

            this.YSpeed = Randomizer.GetRandomNumber(MinimumSpeed, MaximumSpeed);
            this.YSpeed = this.YSpeed*directions[Randomizer.GetRandomNumber(NumberOfDirectionTypes)];
        }

        #endregion
    }
}