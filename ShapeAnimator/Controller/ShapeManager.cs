﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using ShapeAnimator.Model.Factory;
using ShapeAnimator.Model.Shapes;
using ShapeAnimator.Model.Utility;
using ShapeAnimator.Properties;

namespace ShapeAnimator.Controller
{
    /// <summary>
    ///     Manages the collection of shapes on the canvas.
    /// </summary>
    public class ShapeManager
    {
        #region Instance variables

        private readonly PictureBox canvas;

        private List<Shape> shapes;

        #endregion

        #region Constructors

        /// <summary>
        ///     Prevents a default instance of the <see cref="ShapeManager" /> class from being created.
        ///     shapes == new List of type Shape.
        /// </summary>
        private ShapeManager()
        {
            this.shapes = new List<Shape>();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ShapeManager" /> class.
        ///     Precondition: pictureBox != null
        ///     Postcondition: canvas == pictureBox
        /// </summary>
        /// <param name="pictureBox">The picture box that will be drawing on</param>
        public ShapeManager(PictureBox pictureBox) : this()
        {
            if (pictureBox == null)
            {
                throw new ArgumentNullException("pictureBox", Resources.PictureBoxNullMessage);
            }

            this.canvas = pictureBox;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Places random shapes on Canvas.
        ///     Precondition: none
        ///     Postcondition: shapes == shapes + the randomly created shapes.
        /// </summary>
        /// <param name="numberOfShapes">the number of shapes</param>
        public void PlaceRandomShapesOnCanvas(int numberOfShapes)
        {
            for (int i = 0; i < numberOfShapes; i++)
            {
                this.shapes.Add(ShapeFactory.CreateRandomShape());
                this.setShapesLocation();
            }
        }

        /// <summary>
        ///     Places circles on Canvas.
        ///     Precondition: none
        ///     Postcondition: shapes == shapes + the created circles.
        /// </summary>
        /// <param name="numberOfCircles">the number of circles</param>
        public void PlaceCirclesOnCanvas(int numberOfCircles)
        {
            for (int i = 0; i < numberOfCircles; i++)
            {
                this.shapes.Add(ShapeFactory.CreateACircle());
                this.setShapesLocation();
            }
        }

        /// <summary>
        ///     Places spottedcircles on Canvas.
        ///     Precondition: none
        ///     Postcondition: shapes == shapes + the created spotted circles.
        /// </summary>
        /// <param name="numberOfSpottedCircles">the number of spottedCircles</param>
        public void PlaceCirclesSpottedOnCanvas(int numberOfSpottedCircles)
        {
            for (int i = 0; i < numberOfSpottedCircles; i++)
            {
                this.shapes.Add(ShapeFactory.CreateASpottedCircle());
                this.setShapesLocation();
            }
        }

        /// <summary>
        ///     Places squares on Canvas.
        ///     Precondition: none
        ///     Postcondition: shapes == shapes + the created squares.
        /// </summary>
        /// <param name="numberOfSquares">the number of squares</param>
        public void PlaceSquaresOnCanvas(int numberOfSquares)
        {
            for (int i = 0; i < numberOfSquares; i++)
            {
                this.shapes.Add(ShapeFactory.CreateASquare());
                this.setShapesLocation();
            }
        }

        /// <summary>
        ///     Animates the shapes on the Canvas.
        ///     Precondition: none
        /// </summary>
        /// <summary>
        ///     Moves the shapes around and the calls the Shape::Paint method to draw the shapes.
        ///     Precondition: g != null
        /// </summary>
        /// <param name="g">The graphics object to draw on</param>
        public void Update(Graphics g)
        {
            if (g == null)
            {
                throw new ArgumentNullException("g");
            }

            if (this.shapes != null)
            {
                this.animateShapes(g);
            }
        }

        /// <summary>
        ///     Gets the shapes.
        /// </summary>
        /// <returns>The shape manager's collection of shapes</returns>
        public List<Shape> GetShapes()
        {
            return this.shapes;
        }

        /// <summary>
        ///     Empties the shapes from the list of shapes.
        ///     Postcondition: shapes == new List of type Shape.
        /// </summary>
        public void EmptyShapes()
        {
            this.shapes = new List<Shape>();
        }

        private void setShapesLocation()
        {
            foreach (Shape shape in this.shapes)
            {
                int x = Randomizer.GetRandomNumber(this.canvas.Width - shape.Width);
                int y = Randomizer.GetRandomNumber(this.canvas.Height - shape.Height);

                shape.X = x;
                shape.Y = y;
            }
        }

        private void animateShapes(Graphics g)
        {
            foreach (Shape shape in this.shapes)
            {
                shape.Paint(g);
                shape.BounceInOppositeDirectionAtCanvasBounds(this.canvas);
                shape.Move();
            }
        }

        #endregion
    }
}