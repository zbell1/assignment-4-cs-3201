using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using ShapeAnimator.Controller;
using ShapeAnimator.Model.Shapes;
using ShapeAnimator.Model.Utility;

namespace ShapeAnimator.View.Forms
{
    /// <summary>
    ///     Manages the form that will display shapes.
    /// </summary>
    public partial class ShapeAnimatorForm
    {
        #region Instance variables

        private readonly ShapeManager shapeManager;

        #endregion

        #region Constants

        private const int MaximumMilisecondsBeforeTimerTick = 500;

        private const int MinimumMiliSecondsBeforeTimerTick = 1;

        private const int SpeedBarTickFrequencey = 50;

        #endregion

        #region Properties

        /// <summary>
        ///     Converts the text in the numberShapesTextBox to an integer. If the text
        ///     is not convertable to an integer value it returns -1.
        /// </summary>
        public int NumberShapes
        {
            get
            {
                try
                {
                    return Convert.ToInt32(this.numberShapesTextBox.Text);
                }
                catch (Exception)
                {
                    return -1;
                }
            }
        }

        /// <summary>
        ///     Converts the text in the circleShapesTextBox to an integer. If the text
        ///     is not convertable to an integer value it returns -1.
        /// </summary>
        public int NumberCircles
        {
            get
            {
                try
                {
                    return Convert.ToInt32(this.circleTextBox.Text);
                }
                catch (Exception)
                {
                    return -1;
                }
            }
        }

        /// <summary>
        ///     Converts the text in the spottedCircleShapesTextBox to an integer. If the text
        ///     is not convertable to an integer value it returns -1.
        /// </summary>
        public int NumberSpottedCircles
        {
            get
            {
                try
                {
                    return Convert.ToInt32(this.spottedCircleTextBox.Text);
                }
                catch (Exception)
                {
                    return -1;
                }
            }
        }

        /// <summary>
        ///     Converts the text in the squareTextBox to an integer. If the text
        ///     is not convertable to an integer value it returns -1.
        /// </summary>
        public int NumberSquares
        {
            get
            {
                try
                {
                    return Convert.ToInt32(this.squareTextBox.Text);
                }
                catch (Exception)
                {
                    return -1;
                }
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="ShapeAnimatorForm" /> class.
        ///     Precondition: None
        ///     Postcondition: shapeManager == new ShapeManager.
        /// </summary>
        public ShapeAnimatorForm()
        {
            this.InitializeComponent();

            this.setInitialEnablementOfControls();

            this.setUpSpeedBar();

            this.shapeManager = new ShapeManager(this.canvasPictureBox);
        }

        #endregion

        #region Event generated methods

        private void animationTimer_Tick(object sender, EventArgs e)
        {
            this.Refresh();
        }

        private void shapeCanvasPictureBox_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            this.shapeManager.Update(g);
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            this.disableAllTextBoxes();

            this.placeShapesOnCanvas();

            this.placeShapeDataOnTextBox(ShapeSorter.SortShapes(this.shapeManager.GetShapes()));

            this.animationTimer.Start();

            this.setStartButtonEnablementOfControls();
        }

        private void pauseButton_Click(object sender, EventArgs e)
        {
            this.animationTimer.Stop();

            this.pauseButton.Enabled = false;

            this.resumeButton.Enabled = true;
        }

        private void resumeButton_Click(object sender, EventArgs e)
        {
            this.animationTimer.Start();

            this.resumeButton.Enabled = false;

            this.pauseButton.Enabled = true;
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            this.enableAllTextBoxes();

            this.animationTimer.Stop();

            this.shapeManager.EmptyShapes();

            this.clearTextBoxes();

            this.Refresh();

            this.setInitialEnablementOfControls();

            this.animationSpeedBar.Value = MaximumMilisecondsBeforeTimerTick;
        }

        private void animationSpeedBar_Scroll(object sender, EventArgs e)
        {
            this.animationTimer.Interval = this.animationSpeedBar.Value;
        }

        #endregion

        #region Methods

        private void placeShapesOnCanvas()
        {
            this.shapeManager.PlaceRandomShapesOnCanvas(this.NumberShapes);

            this.shapeManager.PlaceCirclesOnCanvas(this.NumberCircles);

            this.shapeManager.PlaceCirclesSpottedOnCanvas(this.NumberSpottedCircles);

            this.shapeManager.PlaceSquaresOnCanvas(this.NumberSquares);
        }

        private void placeShapeDataOnTextBox(IEnumerable<Shape> shapes)
        {
            this.shapeOutputTextBox.Clear();

            this.shapeOutputTextBox.Text = string.Format("{0,-20}  {1,-25}  {2,-20} {3, -10} {4,25}  ", "Shape", "Color",
                "Perimeter", "Area", "Collision count");

            this.shapeOutputTextBox.Text += Environment.NewLine;
            this.shapeOutputTextBox.Text += Environment.NewLine;

            foreach (Shape currentShape in shapes)
            {
                this.addShapeInformationToTextBox(currentShape);
            }
        }

        private void addShapeInformationToTextBox(Shape currentShape)
        {
            this.shapeOutputTextBox.Text += String.Format("{0, -20}  {1, -25}  {2, -20} {3, -10} {4,15}  ",
                currentShape.GetType().Name,
                this.displayShapeRgb(currentShape),
                currentShape.Perimeter, currentShape.Area,
                currentShape.CollisionCount);

            this.shapeOutputTextBox.Text += Environment.NewLine;
        }

        private String displayShapeRgb(Shape currentShape)
        {
            return "(" + currentShape.TheColor.R + ", " + currentShape.TheColor.G + ", " + currentShape.TheColor.B + ")";
        }

        private void setInitialEnablementOfControls()
        {
            this.pauseButton.Enabled = false;

            this.resumeButton.Enabled = false;

            this.clearButton.Enabled = false;

            this.animationSpeedBar.Enabled = false;

            this.startButton.Enabled = true;
        }

        private void disableAllTextBoxes()
        {
            this.numberShapesTextBox.Enabled = false;

            this.circleTextBox.Enabled = false;

            this.spottedCircleTextBox.Enabled = false;

            this.squareTextBox.Enabled = false;
        }

        private void enableAllTextBoxes()
        {
            this.numberShapesTextBox.Enabled = true;

            this.circleTextBox.Enabled = true;

            this.spottedCircleTextBox.Enabled = true;

            this.squareTextBox.Enabled = true;
        }

        private void setStartButtonEnablementOfControls()
        {
            this.startButton.Enabled = false;

            this.pauseButton.Enabled = true;

            this.clearButton.Enabled = true;

            this.animationSpeedBar.Enabled = true;
        }

        private void setUpSpeedBar()
        {
            this.animationSpeedBar.SetRange(MinimumMiliSecondsBeforeTimerTick, MaximumMilisecondsBeforeTimerTick);

            this.animationSpeedBar.RightToLeft = RightToLeft.Yes;

            this.animationSpeedBar.TickStyle = TickStyle.TopLeft;

            this.animationSpeedBar.TickFrequency = SpeedBarTickFrequencey;

            this.animationSpeedBar.Value = MaximumMilisecondsBeforeTimerTick;
        }

        private void clearTextBoxes()
        {
            this.numberShapesTextBox.ResetText();

            this.circleTextBox.ResetText();

            this.spottedCircleTextBox.ResetText();

            this.squareTextBox.ResetText();

            this.shapeOutputTextBox.ResetText();
        }

        #endregion
    }
}