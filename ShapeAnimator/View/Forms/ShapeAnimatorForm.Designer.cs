using System.Windows.Forms;
using ShapeAnimator.View.Forms;

namespace ShapeAnimator.View.Forms
{
    partial class ShapeAnimatorForm : Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.canvasPictureBox = new System.Windows.Forms.PictureBox();
            this.animationTimer = new System.Windows.Forms.Timer(this.components);
            this.numberOfShapesLabel = new System.Windows.Forms.Label();
            this.numberShapesTextBox = new System.Windows.Forms.TextBox();
            this.startButton = new System.Windows.Forms.Button();
            this.circleTextBox = new System.Windows.Forms.TextBox();
            this.squareTextBox = new System.Windows.Forms.TextBox();
            this.spottedCircleTextBox = new System.Windows.Forms.TextBox();
            this.numberOfCirclesLabel = new System.Windows.Forms.Label();
            this.numberOfSpottedCirclesLabel = new System.Windows.Forms.Label();
            this.numberOfSquaresLabel = new System.Windows.Forms.Label();
            this.pauseButton = new System.Windows.Forms.Button();
            this.resumeButton = new System.Windows.Forms.Button();
            this.clearButton = new System.Windows.Forms.Button();
            this.animationSpeedBar = new System.Windows.Forms.TrackBar();
            this.shapeOutputTextBox = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.canvasPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.animationSpeedBar)).BeginInit();
            this.SuspendLayout();
            // 
            // canvasPictureBox
            // 
            this.canvasPictureBox.BackColor = System.Drawing.Color.Black;
            this.canvasPictureBox.Location = new System.Drawing.Point(31, 105);
            this.canvasPictureBox.Name = "canvasPictureBox";
            this.canvasPictureBox.Size = new System.Drawing.Size(720, 480);
            this.canvasPictureBox.TabIndex = 0;
            this.canvasPictureBox.TabStop = false;
            this.canvasPictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.shapeCanvasPictureBox_Paint);
            // 
            // animationTimer
            // 
            this.animationTimer.Interval = 200;
            this.animationTimer.Tick += new System.EventHandler(this.animationTimer_Tick);
            // 
            // numberOfShapesLabel
            // 
            this.numberOfShapesLabel.AutoSize = true;
            this.numberOfShapesLabel.Location = new System.Drawing.Point(12, 21);
            this.numberOfShapesLabel.Name = "numberOfShapesLabel";
            this.numberOfShapesLabel.Size = new System.Drawing.Size(96, 13);
            this.numberOfShapesLabel.TabIndex = 1;
            this.numberOfShapesLabel.Text = "Number of shapes:";
            // 
            // numberShapesTextBox
            // 
            this.numberShapesTextBox.Location = new System.Drawing.Point(113, 16);
            this.numberShapesTextBox.Name = "numberShapesTextBox";
            this.numberShapesTextBox.Size = new System.Drawing.Size(47, 20);
            this.numberShapesTextBox.TabIndex = 2;
            // 
            // startButton
            // 
            this.startButton.Location = new System.Drawing.Point(15, 65);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(75, 23);
            this.startButton.TabIndex = 3;
            this.startButton.Text = "Start";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // circleTextBox
            // 
            this.circleTextBox.Location = new System.Drawing.Point(264, 16);
            this.circleTextBox.Name = "circleTextBox";
            this.circleTextBox.Size = new System.Drawing.Size(47, 20);
            this.circleTextBox.TabIndex = 4;
            // 
            // squareTextBox
            // 
            this.squareTextBox.Location = new System.Drawing.Point(608, 16);
            this.squareTextBox.Name = "squareTextBox";
            this.squareTextBox.Size = new System.Drawing.Size(47, 20);
            this.squareTextBox.TabIndex = 5;
            // 
            // spottedCircleTextBox
            // 
            this.spottedCircleTextBox.Location = new System.Drawing.Point(450, 16);
            this.spottedCircleTextBox.Name = "spottedCircleTextBox";
            this.spottedCircleTextBox.Size = new System.Drawing.Size(47, 20);
            this.spottedCircleTextBox.TabIndex = 6;
            // 
            // numberOfCirclesLabel
            // 
            this.numberOfCirclesLabel.AutoSize = true;
            this.numberOfCirclesLabel.Location = new System.Drawing.Point(166, 21);
            this.numberOfCirclesLabel.Name = "numberOfCirclesLabel";
            this.numberOfCirclesLabel.Size = new System.Drawing.Size(92, 13);
            this.numberOfCirclesLabel.TabIndex = 7;
            this.numberOfCirclesLabel.Text = "Number of circles:";
            // 
            // numberOfSpottedCirclesLabel
            // 
            this.numberOfSpottedCirclesLabel.AutoSize = true;
            this.numberOfSpottedCirclesLabel.Location = new System.Drawing.Point(317, 21);
            this.numberOfSpottedCirclesLabel.Name = "numberOfSpottedCirclesLabel";
            this.numberOfSpottedCirclesLabel.Size = new System.Drawing.Size(127, 13);
            this.numberOfSpottedCirclesLabel.TabIndex = 8;
            this.numberOfSpottedCirclesLabel.Text = "Number of spottedcircles:";
            // 
            // numberOfSquaresLabel
            // 
            this.numberOfSquaresLabel.AutoSize = true;
            this.numberOfSquaresLabel.Location = new System.Drawing.Point(503, 21);
            this.numberOfSquaresLabel.Name = "numberOfSquaresLabel";
            this.numberOfSquaresLabel.Size = new System.Drawing.Size(99, 13);
            this.numberOfSquaresLabel.TabIndex = 9;
            this.numberOfSquaresLabel.Text = "Number of squares:";
            // 
            // pauseButton
            // 
            this.pauseButton.Location = new System.Drawing.Point(96, 65);
            this.pauseButton.Name = "pauseButton";
            this.pauseButton.Size = new System.Drawing.Size(75, 23);
            this.pauseButton.TabIndex = 10;
            this.pauseButton.Text = "Pause";
            this.pauseButton.UseVisualStyleBackColor = true;
            this.pauseButton.Click += new System.EventHandler(this.pauseButton_Click);
            // 
            // resumeButton
            // 
            this.resumeButton.Location = new System.Drawing.Point(177, 65);
            this.resumeButton.Name = "resumeButton";
            this.resumeButton.Size = new System.Drawing.Size(75, 23);
            this.resumeButton.TabIndex = 11;
            this.resumeButton.Text = "Resume";
            this.resumeButton.UseVisualStyleBackColor = true;
            this.resumeButton.Click += new System.EventHandler(this.resumeButton_Click);
            // 
            // clearButton
            // 
            this.clearButton.Location = new System.Drawing.Point(691, 14);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(75, 23);
            this.clearButton.TabIndex = 12;
            this.clearButton.Text = "Clear";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // animationSpeedBar
            // 
            this.animationSpeedBar.Location = new System.Drawing.Point(320, 54);
            this.animationSpeedBar.Name = "animationSpeedBar";
            this.animationSpeedBar.Size = new System.Drawing.Size(455, 45);
            this.animationSpeedBar.TabIndex = 13;
            this.animationSpeedBar.Scroll += new System.EventHandler(this.animationSpeedBar_Scroll);
            // 
            // shapeOutputTextBox
            // 
            this.shapeOutputTextBox.BackColor = System.Drawing.Color.White;
            this.shapeOutputTextBox.Location = new System.Drawing.Point(757, 105);
            this.shapeOutputTextBox.Multiline = true;
            this.shapeOutputTextBox.Name = "shapeOutputTextBox";
            this.shapeOutputTextBox.ReadOnly = true;
            this.shapeOutputTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.shapeOutputTextBox.Size = new System.Drawing.Size(303, 480);
            this.shapeOutputTextBox.TabIndex = 14;
            this.shapeOutputTextBox.TabStop = false;
            this.shapeOutputTextBox.WordWrap = false;
            // 
            // ShapeAnimatorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1072, 620);
            this.Controls.Add(this.shapeOutputTextBox);
            this.Controls.Add(this.animationSpeedBar);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.resumeButton);
            this.Controls.Add(this.pauseButton);
            this.Controls.Add(this.numberOfSquaresLabel);
            this.Controls.Add(this.numberOfSpottedCirclesLabel);
            this.Controls.Add(this.numberOfCirclesLabel);
            this.Controls.Add(this.spottedCircleTextBox);
            this.Controls.Add(this.squareTextBox);
            this.Controls.Add(this.circleTextBox);
            this.Controls.Add(this.startButton);
            this.Controls.Add(this.numberShapesTextBox);
            this.Controls.Add(this.numberOfShapesLabel);
            this.Controls.Add(this.canvasPictureBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "ShapeAnimatorForm";
            this.Text = "Shape Animator A4 by Bell and Smith";
            ((System.ComponentModel.ISupportInitialize)(this.canvasPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.animationSpeedBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox canvasPictureBox;
        private System.Windows.Forms.Timer animationTimer;
        private System.Windows.Forms.Label numberOfShapesLabel;
        private System.Windows.Forms.TextBox numberShapesTextBox;
        private System.Windows.Forms.Button startButton;
        private TextBox circleTextBox;
        private TextBox squareTextBox;
        private TextBox spottedCircleTextBox;
        private Label numberOfCirclesLabel;
        private Label numberOfSpottedCirclesLabel;
        private Label numberOfSquaresLabel;
        private Button pauseButton;
        private Button resumeButton;
        private Button clearButton;
        private TrackBar animationSpeedBar;
        private TextBox shapeOutputTextBox;
    }
}

