﻿using System;
using System.Drawing;
using ShapeAnimator.Model.Shapes;

namespace ShapeAnimator.View.Shapes
{
    /// <summary>
    ///     Responsible for drawing the actual sprite on screen.
    /// </summary>
    public abstract class ShapeSprite
    {
        #region Instance variables

        private readonly SolidBrush randomColorBrush;
        private readonly Shape theShape;

        #endregion

        #region Properties       

        /// <summary>
        ///     Gets the shape.
        /// </summary>
        /// <value>
        ///     The shape.
        /// </value>
        protected Shape TheShape
        {
            get { return this.theShape; }
        }

        /// <summary>
        ///     Gets the random color brush.
        /// </summary>
        /// <value>
        ///     The random color brush.
        /// </value>
        protected SolidBrush RandomColorBrush
        {
            get { return this.randomColorBrush; }
        }

        #endregion

        #region Constructors

        /// <summary>
        ///     Prevents a default instance of the <see cref="ShapeSprite" /> class from being created.
        ///     Postcondition theShape == null;
        ///     Postcondition randomColorBrush == null;
        /// </summary>
        protected ShapeSprite()
        {
            this.theShape = null;
            this.randomColorBrush = null;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ShapeSprite" /> class.
        ///     Precondition: shape != null
        ///     Postcondition: theShape == shape
        ///     Postcondition: randomColorBrush == new SolidBrush
        /// </summary>
        /// <param name="shape">The shape.</param>
        protected ShapeSprite(Shape shape)
        {
            if (shape == null)
            {
                throw new ArgumentNullException("shape");
            }

            this.theShape = shape;

            this.randomColorBrush = new SolidBrush(this.theShape.TheColor);
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Draws a shape
        ///     Preconditon: g != null
        /// </summary>
        /// <param name="g">The graphics object used to draw the shape</param>
        public abstract void Paint(Graphics g);

        #endregion
    }
}