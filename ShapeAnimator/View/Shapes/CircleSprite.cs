﻿using System;
using System.Drawing;
using ShapeAnimator.Model.Shapes;

namespace ShapeAnimator.View.Shapes
{
    /// <summary>
    ///     Draws a circle on screen.
    /// </summary>
    public class CircleSprite : ShapeSprite
    {
        #region Constructors

        /// <summary>
        ///     Prevents a default instance of the <see cref="ShapeAnimator.View.Shapes.CircleSprite" /> class from being created.
        /// </summary>
        public CircleSprite()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ShapeAnimator.View.Shapes.CircleSprite" /> class.
        ///     Precondition: shape != null
        ///     Postcondition: theShape == shape.
        /// </summary>
        /// <param name="shape">The shape.</param>
        public CircleSprite(Shape shape) : base(shape)
        {
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Draws a circle.
        ///     Preconditon: g != null
        /// </summary>
        /// <param name="g">The graphics object used to draw the shape</param>
        public override void Paint(Graphics g)
        {
            if (g == null)
            {
                throw new ArgumentNullException("g");
            }

            if (this.TheShape == null)
            {
                return;
            }

            g.FillEllipse(this.RandomColorBrush, this.TheShape.X, this.TheShape.Y, this.TheShape.Width,
                this.TheShape.Height);
        }

        #endregion
    }
}