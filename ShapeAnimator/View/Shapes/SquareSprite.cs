﻿using System;
using System.Drawing;
using ShapeAnimator.Model.Shapes;

namespace ShapeAnimator.View.Shapes
{
    /// <summary>
    ///     Draws a square on screen.
    /// </summary>
    public class SquareSprite : ShapeSprite
    {
        #region Constructors

        /// <summary>
        ///     Prevents a default instance of the <see cref="ShapeAnimator.View.Shapes.SquareSprite" /> class from being
        ///     created.
        /// </summary>
        public SquareSprite()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ShapeAnimator.View.Shapes.SquareSprite" /> class.
        ///     Precondition: shape != null
        ///     Postcondition: theShape == shape.
        /// </summary>
        /// <param name="shape">The shape.</param>
        public SquareSprite(Shape shape) : base(shape)
        {
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Draws a square.
        ///     Preconditon: g != null
        /// </summary>
        /// <param name="g">The graphics object used to draw the shape</param>
        public override void Paint(Graphics g)
        {
            if (g == null)
            {
                throw new ArgumentNullException("g");
            }

            if (this.TheShape == null)
            {
                return;
            }

            g.FillRectangle(this.RandomColorBrush, this.TheShape.X, this.TheShape.Y, this.TheShape.Width,
                this.TheShape.Height);
        }

        #endregion
    }
}