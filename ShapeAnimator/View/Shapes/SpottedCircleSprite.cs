﻿using System.Drawing;
using ShapeAnimator.Model.Shapes;

namespace ShapeAnimator.View.Shapes
{
    /// <summary>
    ///     Draws a spotted circle sprite on screen.
    /// </summary>
    public class SpottedCircleSprite : CircleSprite
    {
        #region Constants

        private const int SpotSizeScaling = 5;
        private const int DecreaseSpotXyScaling = 4;
        private const int IncreaseSpotXyScaling = 2;

        #endregion

        #region Properties

        private int SpotBottomYOnShape { get; set; }

        private int SpotHeight { get; set; }

        private int SpotLeftXOnShape { get; set; }

        private int SpotRightXOnShape { get; set; }

        private int SpotTopYOnShape { get; set; }

        private int SpotWidth { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Prevents a default instance of the <see cref="ShapeAnimator.View.Shapes.SpottedCircleSprite" /> class from being
        ///     created.
        /// </summary>
        public SpottedCircleSprite()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ShapeAnimator.View.Shapes.SpottedCircleSprite" /> class.
        ///     Precondition: shape != null
        ///     Postcondition: theShape == shape.
        /// </summary>
        /// <param name="shape">The shape.</param>
        public SpottedCircleSprite(Shape shape) : base(shape)
        {
            this.setSpotSize();

            this.scaleSpotSizeWithCircleSize();
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Draws a shape
        ///     Preconditon: g != null.
        /// </summary>
        /// <param name="g">The graphics object used to draw the shape</param>
        public override void Paint(Graphics g)
        {
            base.Paint(g);
            this.paintSpots(g);
        }

        /// <summary>
        ///     Paints the spots.
        ///     Precondition: g != null.
        /// </summary>
        /// <param name="g">The graphics object used to draw the spots</param>
        private void paintSpots(Graphics g)
        {
            var greenBrush = new SolidBrush(Color.Green);

            this.paintTopLeftSpot(greenBrush, g);

            this.paintTopRightSpot(greenBrush, g);

            this.paintBottomLeftSpot(greenBrush, g);

            this.paintBottomRightSpot(greenBrush, g);
        }

        private void setSpotSize()
        {
            this.SpotWidth = (this.TheShape.Width/SpotSizeScaling);

            this.SpotHeight = (this.TheShape.Height/SpotSizeScaling);
        }

        private void scaleSpotSizeWithCircleSize()
        {
            this.SpotLeftXOnShape = this.TheShape.Width/DecreaseSpotXyScaling;

            this.SpotRightXOnShape = this.TheShape.Width/IncreaseSpotXyScaling;

            this.SpotTopYOnShape = this.TheShape.Height/DecreaseSpotXyScaling;

            this.SpotBottomYOnShape = this.TheShape.Height/IncreaseSpotXyScaling;
        }

        private void paintTopLeftSpot(SolidBrush greenBrush, Graphics g)
        {
            g.FillEllipse(greenBrush, this.TheShape.X + this.SpotLeftXOnShape,
                this.TheShape.Y + this.SpotTopYOnShape,
                this.SpotWidth, this.SpotHeight);
        }

        private void paintTopRightSpot(SolidBrush greenBrush, Graphics g)
        {
            g.FillEllipse(greenBrush, this.TheShape.X + this.SpotRightXOnShape,
                this.TheShape.Y + this.SpotTopYOnShape,
                this.SpotWidth, this.SpotHeight);
        }

        private void paintBottomLeftSpot(SolidBrush greenBrush, Graphics g)
        {
            g.FillEllipse(greenBrush, this.TheShape.X + this.SpotLeftXOnShape,
                this.TheShape.Y + this.SpotBottomYOnShape,
                this.SpotWidth, this.SpotHeight);
        }

        private void paintBottomRightSpot(SolidBrush greenBrush, Graphics g)
        {
            g.FillEllipse(greenBrush, this.TheShape.X + this.SpotRightXOnShape,
                this.TheShape.Y + this.SpotBottomYOnShape,
                this.SpotWidth, this.SpotHeight);
        }

        #endregion
    }
}